# OMM_seminarski - Vojne bitke

### Prerequisites
- [matplotlib](https://nodejs.org/en/) In terminal: `pip3 install matplotlib`
- [pip3](https://www.educative.io/edpresso/installing-pip3-in-ubuntu) (if needed)

### Startup
Terminal:
* Clone project `git clone https://gitlab.com/phalto/omm_seminarski`
* Repositioning `cd omm_seminarski/src`
* Start  `python3 bitke.py`  
![omm](/uploads/4e94dba985490e6a81f641252214e401/omm.png)
### Contributor
- Kristina Ćetojević 359/2017
- Jovan Rumenić 069/2017
- Tomislav Savatijević 134/2017
