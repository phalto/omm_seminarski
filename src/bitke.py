import matplotlib.pyplot as plt
import numpy as np
import math as m
import decimal


def funcA(x, a, b, A0, B0):
    return (A0 - m.sqrt(a / b) * B0) / 2 * m.e ** (m.sqrt(a * b) * x) + (A0 + m.sqrt(a / b) * B0) / 2 * m.e ** (
                - m.sqrt(a * b) * x)


def funcB(x, a, b, A0, B0):
    return m.sqrt(b / a) * (
                (A0 + m.sqrt(a / b) * B0) / 2 * m.e ** (- m.sqrt(a * b) * x) - (A0 - m.sqrt(a / b) * B0) / 2 * m.e ** (
                    m.sqrt(a * b) * x))


def findBreakPoint(a, b, A0, B0):
    for i in list(float_range(0, 100, '0.1')):
        if funcA(i, a, b, A0, B0) <= 0 or funcB(i, a, b, A0, B0) <= 0:
            return i-0.0000001
    return i


def float_range(start, stop, step):
    while start < stop:
        yield float(start)
        start += decimal.Decimal(step)


def bitke(a, b, A0, B0):
    breakPoint = findBreakPoint(a, b, A0, B0)
    x = np.array(list(float_range(0, breakPoint, '0.00001')))

    A = (A0 - m.sqrt(a / b) * B0) / 2 * m.e ** (m.sqrt(a * b) * x) + (A0 + m.sqrt(a / b) * B0) / 2 * m.e ** (
                - m.sqrt(a * b) * x)
    B = m.sqrt(b / a) * (
                (A0 + m.sqrt(a / b) * B0) / 2 * m.e ** (- m.sqrt(a * b) * x) - (A0 - m.sqrt(a / b) * B0) / 2 * m.e ** (
                    m.sqrt(a * b) * x))

    if b * A0 ** 2 == a * B0 ** 2:
        pobednik = "Nereseno"
    elif b * A0 ** 2 > a * B0 ** 2:
        pobednik = "Pobedila je vojska A"
    else:
        pobednik = "Pobedila je vojska B"

    plt.plot(x, A, label='Vojska A')
    plt.plot(x, B, label='Vojska B')

    plt.legend(loc='upper right')
    plt.xlabel('Vreme')
    plt.ylabel('Broj vojnika')
    plt.title('Vojne bitke')

    print(pobednik)
    bottom, top = plt.ylim()
    print(top, bottom)
    plt.show()


def rat():
    # inputs
    a = float(input("unesite paramtere(a, b, A, B)\n"))
    b = float(input())
    A0 = int(input())
    B0 = int(input())
    bitke(a, b, A0, B0)


rat()

